#pragma once

#include "IPlugin.h"

class PluginA : public IPlugin
{
public:
    std::string getName() const override;
    PluginVersion getVersion() const override;
    void PrintMsg(const std::string &s) override;
};
