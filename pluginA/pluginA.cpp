#include "pluginA.h"

#include <Poco/ClassLibrary.h>
#include <iostream>

POCO_BEGIN_MANIFEST(IPlugin)
   POCO_EXPORT_CLASS(PluginA)
POCO_END_MANIFEST

std::string PluginA::getName() const
{
	return "PluginA";
}

PluginVersion PluginA::getVersion() const
{
   PluginVersion tempVar = PluginVersion();
   tempVar.Major = 0;
   tempVar.Minor = 1;
   return tempVar;
}

void PluginA::PrintMsg(const std::string &s)
{
   std::cout << s << std::endl;
}
