#include "IPlugin.h"

#include <Poco/SharedLibrary.h>
#include <Poco/ClassLoader.h>
#include <Poco/Manifest.h>

#include <iostream>

using Poco::SharedLibrary;

typedef Poco::ClassLoader<IPlugin> PluginLoader;
typedef Poco::Manifest<IPlugin> PluginManifest;

int main()
{
    try
    {
        PluginLoader loader;
        std::string path("./libPluginA");
        path.append(SharedLibrary::suffix());

        loader.loadLibrary(path);

        IPlugin* pPlugableA = loader.create("PluginA");
        pPlugableA->PrintMsg("doing something");

        loader.classFor("PlugInA").autoDelete(pPlugableA);

        loader.unloadLibrary(path);
    }
    catch (Poco::Exception& exc)
    {
        std::cout << exc.displayText() << std::endl;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
